#include "Menu.h"
#include "CVars.h"
#include "Drawings.h"

void CMenu::CheckBox(POINT mouse, const char* text, float x, float y, float w, float h, float value)
{
	Hack.Drawings->DrawString(x + 25.0, y, Color::Black(), false, text);
	Hack.Drawings->DrawRect(x, y, w, h, Hack.Color->colorCheck(value), false);

	if (GetAsyncKeyState(VK_LBUTTON) & 0x1)
	{
		if ((mouse.x > x) && (mouse.y > y) && (mouse.x < (x + w)) && (mouse.y < (y + h)))
		{
			if (value >= 1.0)
			{
				value = 0.0;
			}
			else
			{
				value = 1.0;
			}
		}
	}
}