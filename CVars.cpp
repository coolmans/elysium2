#include "CVars.h"

float GetPrivateProfileFloat(LPCSTR lpAppName, LPCSTR lpKeyName, FLOAT flDefault, LPCSTR lpFileName)
{
	char szData[32];

	GetPrivateProfileStringA(lpAppName, lpKeyName, std::to_string(flDefault).c_str(), szData, 32, lpFileName);

	return (float)atof(szData);
}

void WritePrivateProfileFloat(LPCSTR lpAppName, LPCSTR lpKeyName, FLOAT flValue, LPCSTR lpFileName)
{
	WritePrivateProfileStringA(lpAppName, lpKeyName, std::to_string((int)flValue).c_str(), lpFileName);
}

CVars::CVars(HMODULE hModule)
{
	m_hModule = hModule;
}

CVars::~CVars()
{

}

void CVars::Load()
{
	char szPath[MAX_PATH];
	GetModuleFileNameA(m_hModule, szPath, MAX_PATH);
	std::string path(szPath);





	aimbot_active = GetPrivateProfileFloat("main", "aimbot.active", 0, "C:/elysium2/autoexec.cfg");
	aimbot_key = GetPrivateProfileFloat("main", "aimbot.key", 0, "C:/elysium2/autoexec.cfg");
	aimbot_autoshoot = GetPrivateProfileFloat("main", "aimbot.autoshoot", 0, "C:/elysium2/autoexec.cfg");
	aimbot_bone = GetPrivateProfileFloat("main", "aimbot.bone", 0, "C:/elysium2/autoexec.cfg");
	aimbot_fov = GetPrivateProfileFloat("main", "aimbot.fov", 0, "C:/elysium2/autoexec.cfg");
	aimbot_smooth = GetPrivateProfileFloat("main", "aimbot.smooth", 0, "C:/elysium2/autoexec.cfg");
	aimbot_delay = GetPrivateProfileFloat("main", "aimbot.delay", 0, "C:/elysium2/autoexec.cfg");
	aimbot_time = GetPrivateProfileFloat("main", "aimbot.time", 0, "C:/elysium2/autoexec.cfg");
	aimbot_silent = GetPrivateProfileFloat("main", "aimbot.silent", 0, "C:/elysium2/autoexec.cfg");
	aimbot_autopistol = GetPrivateProfileFloat("main", "aimbot.autopistol", 0, "C:/elysium2/autoexec.cfg");
	esp_active = GetPrivateProfileFloat("main", "esp.active", 0, "C:/elysium2/autoexec.cfg");
	esp_box = GetPrivateProfileFloat("main", "esp.box", 0, "C:/elysium2/autoexec.cfg");
	esp_health = GetPrivateProfileFloat("main", "esp.health", 0, "C:/elysium2/autoexec.cfg");
	esp_name = GetPrivateProfileFloat("main", "esp.name", 0, "C:/elysium2/autoexec.cfg");
	esp_traceline = GetPrivateProfileFloat("main", "esp.traceline", 0, "C:/elysium2/autoexec.cfg");
	chams_active = GetPrivateProfileFloat("main", "chams.active", 0, "C:/elysium2/autoexec.cfg");
	chams_visibleonly = GetPrivateProfileFloat("main", "chams.visibleonly", 0, "C:/elysium2/autoexec.cfg");
	chams_flat = GetPrivateProfileFloat("main", "chams.flat", 0, "C:/elysium2/autoexec.cfg");
	misc_bunnyhop = GetPrivateProfileFloat("main", "misc.bunnyhop", 0, "C:/elysium2/autoexec.cfg");
	misc_autostrafer = GetPrivateProfileFloat("main", "misc.autostrafer", 0, "C:/elysium2/autoexec.cfg");
	misc_autopistol = GetPrivateProfileFloat("main", "misc.autopistol", 0, "C:/elysium2/autoexec.cfg");
	misc_rcs = GetPrivateProfileFloat("main", "misc.rcs", 0, "C:/elysium2/autoexec.cfg");
	misc_rcs_delay = GetPrivateProfileFloat("main", "misc.rcs.delay", 0, "C:/elysium2/autoexec.cfg");
	misc_sniper_crosshair = GetPrivateProfileFloat("main", "misc.sniper.crosshair", 0, "C:/elysium2/autoexec.cfg");
	misc_norecoil = GetPrivateProfileFloat("main", "misc.norecoil", 0, "C:/elysium2/autoexec.cfg");
}