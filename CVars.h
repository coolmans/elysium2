#include "SDK.h"

class CVars
{
public:
	CVars(HMODULE hModule);
	~CVars();

	void Load();

public:
	float aimbot_active;
	float aimbot_key;
	float aimbot_autoshoot;
	float aimbot_bone;
	float aimbot_fov;
	float aimbot_smooth;
	float aimbot_delay;
	float aimbot_time;
	float aimbot_silent;
	float aimbot_autopistol;

	float esp_active;
	float esp_box;
	float esp_health;
	float esp_name;
	float esp_traceline;

	float chams_active;
	float chams_visibleonly;
	float chams_flat;

	float misc_bunnyhop;
	float misc_autostrafer;
	float misc_autopistol;
	float misc_rcs;
	float misc_rcs_delay;
	float misc_sniper_crosshair;
	float misc_norecoil;

	bool menuactive = false;
	bool safemode = true;

private:
	HMODULE m_hModule;
};